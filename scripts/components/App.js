/*
  App
*/

import React from 'react';
import Header from './Header';
import Word from './Word';
import Inventory from './Inventory';
import Catalyst from 'react-catalyst';
import reactMixin from 'react-mixin';
import autobind from 'autobind-decorator';

// Firebase
import Rebase  from 're-base';
var base = Rebase.createClass('https://brilliant-torch-3820.firebaseio.com/');

@autobind
class App extends React.Component {
  
  constructor() {
    super();

    this.state = {
      words : {},
    }
  }

  componentDidMount() {
    base.syncState(this.props.params.nameId + '/words', {
      context : this,
      state : 'words'
    });

  }
  
  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem('order-' + this.props.params.nameId, JSON.stringify(nextState.order));
  }


  addWord(word) {
    var timestamp = (new Date()).getTime();
    // update the state object
    this.state.words['word-' + timestamp] = word;
    // set the state
    this.setState({ words : this.state.words });
  }

  removeWord(key) {
    if(confirm("Are you sure you want to remove this word?!")) {
      this.state.words[key] = null;
      this.setState({
        words : this.state.words
      });
    }
  }

  loadSamples() {
    this.setState({
      words : require('../alien-words')
    });
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue; 
    }

    return array;
  }

  renderWord(key){
    // in the React render function I'm randomising the keys of the State.word object
    return <Word key={key} index={key} details={this.state.words[key]} addToOrder={this.addToOrder}/>
  }

  render() {
    return (
      <div className="catch-of-the-day">
        <div className="menu">
          <Header tagline="Gunji Girl presents" />
          <ul className="list-of-words">
            {
             this.shuffle(Object.keys(this.state.words)).map(this.renderWord)}
          </ul>
        </div>  
        <Inventory addWord={this.addWord} loadSamples={this.loadSamples} words={this.state.words} linkState={this.linkState.bind(this)} removeWord={this.removeWord} />
      </div>
    )
  }

};

reactMixin.onClass(App, Catalyst.LinkedStateMixin);

export default App;
