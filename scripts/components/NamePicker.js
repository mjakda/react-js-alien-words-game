/* 
  StorePicker
  This will let us make <StorePicker/>
*/

import React from 'react';
import { History } from 'react-router';
import h from '../helpers';
import reactMixin from 'react-mixin';
import autobind from 'autobind-decorator';

@autobind
class NamePicker extends React.Component {

  goToGame(event) {
    event.preventDefault();
    // get the data from the input
    var nameId = this.refs.nameId.value;
    this.history.pushState(null, '/gamer/' + nameId);
  }

  render() {
    return (
      <form className="store-selector" onSubmit={this.goToGame}>
        <h2>Please Enter A Name</h2>
        <input type="text" ref="nameId" defaultValue={h.getFunName()} required />
        <input type="Submit" />
      </form>
    )
  }
}

reactMixin.onClass(NamePicker, History);

export default NamePicker;
