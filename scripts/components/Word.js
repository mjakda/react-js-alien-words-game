/*
  Word
  <Word />
*/

import React from 'react';
import h from '../helpers';
import autobind from 'autobind-decorator';

@autobind
class Word extends React.Component {
  
  onButtonClick() {
    console.log("Going to add the fish: ", this.props.index);
    var key = this.props.index;
    this.props.addToOrder(key);
  }
  
  render() {
    var details = this.props.details;
    var isNonsense = (details.nonsense === 'yes' ? true : false);
    var buttonText = (isNonsense ? 'Yes' : 'No!');
    return (
      <li className="menu-fish">
      {details.text}
       
      </li>
    )
  }
};


export default Word;
