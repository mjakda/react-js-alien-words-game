
/*
  Add Fish Form
  <AddWordForm />
*/

import React from 'react';
import autobind from 'autobind-decorator';

@autobind
class AddWordForm extends React.Component {
  createWord(event) {
    // 1. Stop the form from submitting
    event.preventDefault();
    // 2. Take the data from the form and create an object
    var word = {
      text : this.refs.text.value,
      level : this.refs.level.value,
      nonsense : this.refs.nonsense.value,
      desc : this.refs.desc.value,
    }

    // 3. Add the fish to the App State
    this.props.addWord(word);
    this.refs.wordForm.reset();
  }

  render() {
    return (
      <form className="fish-edit" ref="wordForm" onSubmit={this.createWord}>
        <input type="text" ref="text" placeholder="Text"/>
        <input type="text" ref="level" placeholder="Level" />
        <select ref="nonsense">
          <option value="yes">Yes!</option>
          <option value="no">No!</option>
        </select>
        <textarea type="text" ref="desc" placeholder="Desc"></textarea>
        <button type="submit">+ Add Item </button>
      </form>
    )
  }
};

export default AddWordForm;
