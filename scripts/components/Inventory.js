/*
  Inventory
  <Inventory/>
*/

import React from 'react';
import AddWordForm from './AddWordForm';
import autobind from 'autobind-decorator';

@autobind
class Inventory extends React.Component {
  
  renderInventory(key) {
    var linkState = this.props.linkState;
    return (
      <div className="fish-edit" key={key}>
        <input type="text" valueLink={linkState('words.'+ key +'.text')}/>
        <input type="text" valueLink={linkState('words.'+ key +'.level')}/>
        <select valueLink={linkState('words.' + key + '.nonsense')}>
          <option value="yes">Yes!</option>
          <option value="no">No!</option>
        </select>

        <textarea valueLink={linkState('words.' + key + '.desc')}></textarea>
        
        <button onClick={this.props.removeWord.bind(null, key)}>Remove Word</button>
        
      </div>
    )
  }

  render() {
    return (
      <div>
        <h2>Word Library</h2>
        
        {Object.keys(this.props.words).map(this.renderInventory)}

        <AddWordForm {...this.props} />
        <button onClick={this.props.loadSamples}>Load Sample words</button>
      </div>
    )
  }
};

Inventory.propTypes = {
    addWord : React.PropTypes.func.isRequired,
    loadSamples : React.PropTypes.func.isRequired,
    words : React.PropTypes.object.isRequired,
    linkState : React.PropTypes.func.isRequired,
    removeWord : React.PropTypes.func.isRequired
}

export default Inventory;
